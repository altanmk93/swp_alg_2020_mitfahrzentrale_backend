package de.fuberlin.swp2020.mitfahrzentrale.configs;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "cityRoutingEntityManagerFactory", basePackages = {
        "de.fuberlin.swp2020.mitfahrzentrale.repositories.city_routing"}, transactionManagerRef = "cityRoutingTransactionManager")
public class SecondDataSourceConfiguration {

    @Bean(name = "datasource2")
    @ConfigurationProperties(prefix = "spring.datasource.cityrouting")
    public DataSource dataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "cityRoutingEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
            EntityManagerFactoryBuilder builder,
            @Qualifier("datasource2") DataSource dataSource){
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        return builder.dataSource(dataSource).properties(properties)
                .packages("de.fuberlin.swp2020.mitfahrzentrale.entities.city_routing").build();
    }

    @Bean(name = "cityRoutingTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("cityRoutingEntityManagerFactory") EntityManagerFactory entityManagerFactory){
        return new JpaTransactionManager(entityManagerFactory);
    }
}
