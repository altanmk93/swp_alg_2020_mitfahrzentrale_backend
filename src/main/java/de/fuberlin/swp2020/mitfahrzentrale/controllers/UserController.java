package de.fuberlin.swp2020.mitfahrzentrale.controllers;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.AddressRepository;
import de.fuberlin.swp2020.mitfahrzentrale.request.Address.AddressRequest;
import de.fuberlin.swp2020.mitfahrzentrale.request.AuthenticationRequest;
import de.fuberlin.swp2020.mitfahrzentrale.response.AuthenticationResponse;
import de.fuberlin.swp2020.mitfahrzentrale.response.UserResponse;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.UserRepository; 
import de.fuberlin.swp2020.mitfahrzentrale.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("api/v1")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;


    @PostMapping("/user")
    public ResponseEntity<?> register(@RequestBody AuthenticationRequest authenticationRequest) {
        String password = authenticationRequest.getPassword();
        String requestedEmail = authenticationRequest.getEmail();

        boolean usernameExists = userRepository.existsUserByUsername(requestedEmail);

        if (usernameExists) {
            return new ResponseEntity<>("Username already exists", HttpStatus.BAD_REQUEST);
        }

        try {
            User user = new User();
            user.setFirstName(authenticationRequest.getFirstName());
            user.setLastName(authenticationRequest.getLastName());
            user.setEmail(requestedEmail);
            user.setUsername(authenticationRequest.getUsername());
            user.setPassword(passwordEncoder.encode(password));
            user.setCarSeats(authenticationRequest.getCarSeats()); 
            
            AddressRequest homeRequest = authenticationRequest.getHomeAddress();
            AddressRequest officeRequest = authenticationRequest.getOfficeAddress();
            Address homeAddress = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
                    homeRequest.getPostcode(), homeRequest.getStreet(),
                    homeRequest.getStreetNumber(), homeRequest.getCity());

            Address officeAddress = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
                    officeRequest.getPostcode(), officeRequest.getStreet(),
                    officeRequest.getStreetNumber(), officeRequest.getCity());

            user.setHomeAddress(homeAddress);
            user.setOfficeAddress(officeAddress);
            userRepository.save(user);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            return new ResponseEntity<>("Wrong address", HttpStatus.BAD_REQUEST);
        }
        
    }

    @PostMapping("/auth")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
                    authenticationRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Invalid username or password", e);
        }

        final String jwt = jwtUtil.generateToken(authenticationRequest.getEmail());
        final Date expiration = jwtUtil.extractExpiration(jwt);
        UUID userid = userRepository.findByUsername(authenticationRequest.getEmail()).getId();

        return ResponseEntity.ok(new AuthenticationResponse(jwt, expiration, userid ));

    }

    @GetMapping("/user/{userId}")
    @ResponseBody
    public ResponseEntity<?> getUserById(@PathVariable UUID userId){
        User user = userRepository.findById(userId).orElse(null);
        if (user != null){
            UserResponse userResponse = new UserResponse();
            userResponse.setCarseats(user.getCarSeats());
            userResponse.setFirstName(user.getFirstName());
            userResponse.setLastName(user.getLastName());
            userResponse.setId(user.getId());
            userResponse.setOfficeAddress(user.getOfficeAddress());
            userResponse.setHomeAddress(user.getHomeAddress());
            userResponse.setEmail(user.getEmail());
            userResponse.setUserName(user.getUsername());
            return ResponseEntity.ok(userResponse);
        }else {
            return new ResponseEntity<>("user not exists", HttpStatus.BAD_REQUEST);
        }

    }

    @PatchMapping("/user/{userId}")
    @ResponseBody
    public ResponseEntity<?> replaceUserById(@PathVariable UUID userId, @RequestBody AuthenticationRequest request){
        User user = userRepository.findById(userId).orElse(null);
        if (user != null){
            user.setEmail(request.getEmail());
            user.setUsername(request.getEmail());
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            //newUser.getPhoneNumber(request.getPhoneNumber());
            user.setCarSeats(request.getCarSeats());
            user.setCommutes(user.getCommutes());

            Address homeAddress = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
                    request.getHomeAddress().getPostcode(), request.getHomeAddress().getStreet(),
                    request.getHomeAddress().getStreetNumber(), request.getHomeAddress().getCity());

            Address officeAddress = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
                    request.getOfficeAddress().getPostcode(), request.getOfficeAddress().getStreet(),
                    request.getOfficeAddress().getStreetNumber(), request.getOfficeAddress().getCity());

            user.setHomeAddress(homeAddress);
            user.setOfficeAddress(officeAddress);
            userRepository.save(user);
            return ResponseEntity.ok(user);
        }else {
            return new ResponseEntity<>("user not exists", HttpStatus.BAD_REQUEST);
        }
    }


}
