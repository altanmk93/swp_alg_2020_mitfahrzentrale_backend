package de.fuberlin.swp2020.mitfahrzentrale.controllers;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Commute;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.AddressRepository;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.CommuteRepository;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.UserRepository;
import de.fuberlin.swp2020.mitfahrzentrale.request.Commute.*;
import de.fuberlin.swp2020.mitfahrzentrale.util.*;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@CrossOrigin
@RequestMapping("api/v1/")
public class CommuteControllers {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommuteRepository commuteRepository;

    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/commutes")
    public ResponseEntity<?> getCommutes(@RequestParam("userid") UUID userid) {
        User u = userRepository.findById(userid).orElse(null);
        if (u != null) {
            List<Commute> commutes = commuteRepository.findByDriver(u);
  
            commutes.addAll(u.getCommutes());
            return ResponseEntity.ok(commutes);
        } else {
            return new ResponseEntity<>("user not exists", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/commutes")
    public ResponseEntity<?> createCommute(@RequestBody CommuteRequestPost commuteRequest) {

        UUID userid = commuteRequest.getDriverID();
        User u = userRepository.findById(userid).orElse(null);
        Address start = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            commuteRequest.getStart().getPostcode(), commuteRequest.getStart().getStreet(),
            commuteRequest.getStart().getStreetNumber(), commuteRequest.getStart().getCity());

        Address destination = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            commuteRequest.getDestination().getPostcode(), commuteRequest.getDestination().getStreet(),
            commuteRequest.getDestination().getStreetNumber(), commuteRequest.getDestination().getCity());

        if(u == null || start == null || destination == null){
            return new ResponseEntity<>("address or user are wrong", HttpStatus.BAD_REQUEST);
        }

        Commute commute = new Commute();
        commute.setDate(LocalDateTime.now());
        commute.setDriver(u);
        commute.setMaxPassengers(commuteRequest.getMaxPassengers());
        commute.setPassengers(new ArrayList<User>());
        commute.setStartAddress(start);
        commute.setDestinationAddress(destination);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime dateTime = LocalDateTime.parse(commuteRequest.getStartTime(), formatter);
        commute.setStartTime(dateTime);
        commuteRepository.save(commute);
        return ResponseEntity.ok(commute);
    }

    @GetMapping("/commutes/{id}")
    public ResponseEntity<?> getCommute(@PathVariable(value = "id") UUID id) {
        boolean commuteExists = commuteRepository.existsCommuteById(id);
        if (!commuteExists || id == null) {
            return new ResponseEntity<>("commute not exists", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(commuteRepository.findById(id));
    }

    @PatchMapping("/commutes/{id}")
    public ResponseEntity<?> updateCommute(@PathVariable(value = "id") UUID id,
            @RequestBody CommuteRequestPost commuteRequest) {

        Commute commute = commuteRepository.findById(id).orElse(null);
        Address start = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            commuteRequest.getStart().getPostcode(), commuteRequest.getStart().getStreet(),
            commuteRequest.getStart().getStreetNumber(), commuteRequest.getStart().getCity());

        Address destination = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            commuteRequest.getDestination().getPostcode(), commuteRequest.getDestination().getStreet(),
            commuteRequest.getDestination().getStreetNumber(), commuteRequest.getDestination().getCity());

        if (commute == null || start == null || destination == null) {
            return new ResponseEntity<>("commute or address are not exist", HttpStatus.BAD_REQUEST);
        }

        UUID userid = commuteRequest.getDriverID();
        User u = userRepository.findById(userid).orElse(null);
        commute.setDate(LocalDateTime.now());
        commute.setDriver(u);
        commute.setMaxPassengers(commuteRequest.getMaxPassengers()); 
        commute.setDestinationAddress(destination);
        commute.setStartAddress(start);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime dateTime = LocalDateTime.parse(commuteRequest.getStartTime(), formatter);
        commute.setStartTime(dateTime);
        commuteRepository.save(commute);
        return ResponseEntity.ok(commute);

    }

    @DeleteMapping("/commutes/{id}")
    public ResponseEntity<?> deleteCommute(@PathVariable(value = "id" ) UUID id, @RequestParam(value = "userid") UUID userid) {
        Commute commute = commuteRepository.findById(id).orElse(null);
        User user = userRepository.findById(userid).orElse(null);

        if (commute != null && user != null ) {
            if(commute.getDriver().getId() == user.getId()){
                commuteRepository.delete(commute);
                return new ResponseEntity<>("Commute deleted", HttpStatus.OK);
            }
            if(commute.getPassengers().contains(user)){
                List<User> tmp = commute.getPassengers();
                tmp.remove(user);
                commute.setPassengers(tmp);
                commuteRepository.save(commute);
                return new ResponseEntity<>("Passenger deleted", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("user is not in commute", HttpStatus.BAD_REQUEST);
            }

        } else {
            return new ResponseEntity<>("commute not exists", HttpStatus.BAD_REQUEST);
        }

    }

    @PatchMapping("possiblecommutes")
    public ResponseEntity<?> possibleCommutes(@RequestBody PossibleCommutes possibleCommutes) {
        UUID userid = possibleCommutes.getUserid();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime dateTime = LocalDateTime.parse(possibleCommutes.getDate(), formatter);

        Address start = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            possibleCommutes.getStart().getPostcode(), possibleCommutes.getStart().getStreet(),
            possibleCommutes.getStart().getStreetNumber(), possibleCommutes.getStart().getCity());

        Address destination = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            possibleCommutes.getDestination().getPostcode(), possibleCommutes.getDestination().getStreet(),
            possibleCommutes.getDestination().getStreetNumber(), possibleCommutes.getDestination().getCity());


        List<Commute> commutes = commuteRepository.findAll();
        List<Commute> result = new ArrayList<Commute>();

        for (Commute commute : commutes) {
            boolean inCommune = false;
            List<User> passengers = commute.getPassengers();
            for (User u : passengers) {

                if (u.getId() == userid) {
                    inCommune = true;
                    break;
                }
            }

            Address commute_address_dest = commute.getDestinationAddress();
            Address commute_address_start = commute.getStartAddress();
            LocalDateTime commute_start_time = commute.getStartTime();

            double start_delta = CommuteUtil.calculateDistance(commute_address_start.getLatitude(),
                    start.getLatitude(), commute_address_start.getLongitude(), start.getLongitude(), 0, 0);

            double destination_delta = CommuteUtil.calculateDistance(commute_address_dest.getLatitude(),
                    destination.getLatitude(), commute_address_dest.getLongitude(), destination.getLongitude(), 0, 0);

            int year_diff = dateTime.getYear() - commute_start_time.getYear();
            int month_diff = dateTime.getMonthValue() - commute_start_time.getMonthValue();
            int day_diff = dateTime.getDayOfYear() - commute_start_time.getDayOfYear();
            int time_diff = Math.abs(dateTime.getHour() - commute_start_time.getHour());

            if (start_delta < 1000 && destination_delta < 1000 && year_diff == 0 && month_diff == 0 && day_diff == 0
                    && time_diff < 1 && !inCommune) {

                result.add(commute);

            }
        }

        return ResponseEntity.ok(result);

    }

}
