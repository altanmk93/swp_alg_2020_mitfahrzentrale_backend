package de.fuberlin.swp2020.mitfahrzentrale.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.AddressRepository;
import de.fuberlin.swp2020.mitfahrzentrale.request.Services.AddressRequest;
import de.fuberlin.swp2020.mitfahrzentrale.request.Services.CoordinateRequest;
import de.fuberlin.swp2020.mitfahrzentrale.response.routePlannerResponse.Pgr;
import de.fuberlin.swp2020.mitfahrzentrale.services.RoutePlannerService;

@RestController
@CrossOrigin 
@RequestMapping("api/v1/services")
public class ServicesController {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private RoutePlannerService routePlannerService;


    @GetMapping("/address")
    public Address getAddress(@RequestBody AddressRequest coordinates) {
        return addressRepository.findByLatitudeAndLongitude(coordinates.getLatitude(), coordinates.getLongitutde());
    }

    @GetMapping("/coordinates")
    public Address getCoordinates(@RequestBody CoordinateRequest address) {
        return addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(address.getPostcode(),
                address.getStreet(), address.getStreetNumber(), address.getCity());
    }

    @GetMapping("/pgrSite")
    @ResponseBody
    public ResponseEntity<?> pgrSite(@RequestParam(value = "sourceLat", defaultValue = "52.505") BigDecimal sLat,
                       @RequestParam(value = "sourceLon", defaultValue = "13.399") BigDecimal sLon,
                       @RequestParam(value = "targetLat", defaultValue = "52.448") BigDecimal tLat,
                       @RequestParam(value = "targetLon", defaultValue = "13.339") BigDecimal tLon,
                       @RequestParam(value = "nodeDeltaFilter", defaultValue = "0.0") double nodeDeltaFilter) {

        try {
            Pgr route = routePlannerService.compute_pgr(sLat, sLon, tLat, tLon, nodeDeltaFilter);
            return ResponseEntity.ok(route) ;
            
        } catch (Exception e) {
            return new ResponseEntity<>("PGROUTING error", HttpStatus.BAD_REQUEST);
            
        }
        
    }

}