package de.fuberlin.swp2020.mitfahrzentrale.controllers;

 

 
import java.time.LocalDateTime; 
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Commute;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.CommuteRequest;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.AddressRepository;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.CommuteRepository;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.CommuteRequestRepository;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.UserRepository;
import de.fuberlin.swp2020.mitfahrzentrale.request.Commute.CommuteRequestPatch;
import de.fuberlin.swp2020.mitfahrzentrale.request.Commute.CommuteRequestPostId; 
 

@RestController
@CrossOrigin 
@RequestMapping("api/v1/")
public class CommuteRequestController {

    @Autowired
    private CommuteRequestRepository commuteRequestRepository;

    @Autowired
    private CommuteRepository commuteRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;


    @GetMapping("/commutes/request/commute/{id}")
    public ResponseEntity<?> getRequestsByCommute(@PathVariable(value = "id") UUID id){

        Commute c = commuteRepository.findById(id).orElse(null);
        if(c == null){
            return new  ResponseEntity<>("commute does not exists", HttpStatus.BAD_REQUEST);
        }
        List<CommuteRequest> result = commuteRequestRepository.findByCommute(c);
        return ResponseEntity.ok(result);

    }

    @PostMapping("/commutes/request/{id}")
    public ResponseEntity<?> postRequestsforCommute(@PathVariable(value = "id") UUID id , @RequestBody CommuteRequestPostId commuteRequest){
        Commute c = commuteRepository.findById(id).orElse(null);
        UUID userid = commuteRequest.getUserid(); 
        User user = userRepository.findById(userid).orElse(null);

        Address start = addressRepository.findByPostcodeAndStreetAndStreetNumberAndCity(
            commuteRequest.getStart().getPostcode(), commuteRequest.getStart().getStreet(),
            commuteRequest.getStart().getStreetNumber(), commuteRequest.getStart().getCity());
 
        if(c == null || user == null || start == null ){
            return new  ResponseEntity<>("Wrong input!", HttpStatus.BAD_REQUEST);
        }
        CommuteRequest request = new CommuteRequest();
        request.setAddress(start);
        request.setCommute(c);
        request.setCreatedAt(LocalDateTime.now());
        request.setStatus("requested");
        request.setUser(user);

        //update
        List<CommuteRequest> requests =  c.getCommuteRequests();
        requests.add(request);
        c.setCommuteRequests(requests);

        commuteRepository.save(c);
        commuteRequestRepository.save(request);
        return ResponseEntity.ok(request);
    }

    @GetMapping("/commutes/request/user/{id}")
    public ResponseEntity<?> getRequestsByUser(@PathVariable(value = "id") UUID id){

        User u = userRepository.findById(id).orElse(null);
        if(u == null){
            return new  ResponseEntity<>("Wrong input!", HttpStatus.BAD_REQUEST);
        }

        List<CommuteRequest> list = commuteRequestRepository.findByUser(u); 
        return ResponseEntity.ok(list);
 
    }

    @PatchMapping("/commutes/request/user/{id}")
    public ResponseEntity<?> patchRequests(@PathVariable(value = "id") UUID id, @RequestBody CommuteRequestPatch request){

        String state = request.getState();
        CommuteRequest cr = commuteRequestRepository.findById(id).orElse(null);

        if( cr == null || ( ! state.equals("accepted") &&  ! state.equals("rejected") )  ){
            return new  ResponseEntity<>("Wrong id", HttpStatus.BAD_REQUEST);
        }
        User u = cr.getUser();
        Commute c = cr.getCommute();
        int countCurrentpassengers = c.getPassengers().size();
        int maxPassengers = c.getMaxPassengers();
        boolean notAlreadyIn = c.getPassengers().contains(u);

        if(notAlreadyIn){
            return new  ResponseEntity<>("user already added", HttpStatus.BAD_REQUEST);
        }

        if(maxPassengers < countCurrentpassengers){
            return new ResponseEntity<>("commute is full", HttpStatus.BAD_REQUEST);
        }

        if(state.equals("accepted")){
            List<User> tmp =  c.getPassengers();
            tmp.add(u);
            c.setPassengers(tmp);
            List <Commute> tmp1 =  u.getCommutes();
            tmp1.add(c);
            u.setCommutes(tmp1);
        }
        
        cr.setStatus(state); 
        userRepository.save(u);
        commuteRepository.save(c);
        commuteRequestRepository.save(cr);
        return ResponseEntity.ok(cr);
    }

    @DeleteMapping("/commutes/request/{id}")
    public ResponseEntity<?> deleteRequest(@PathVariable(value = "id") UUID id){
        CommuteRequest cr = commuteRequestRepository.findById(id).orElse(null);
        if( cr == null ){
            return new  ResponseEntity<>("Wrong id", HttpStatus.BAD_REQUEST);
        }
        commuteRequestRepository.delete(cr);
        return ResponseEntity.ok(cr);
    }
    
}