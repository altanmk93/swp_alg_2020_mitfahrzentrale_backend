package de.fuberlin.swp2020.mitfahrzentrale.repositories.db1;

  
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Commute;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.CommuteRequest;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;

public interface CommuteRequestRepository extends JpaRepository<CommuteRequest, UUID> {

    List<CommuteRequest> findByCommute(Commute c);

    List<CommuteRequest> findByUser(User u);

    

 



}