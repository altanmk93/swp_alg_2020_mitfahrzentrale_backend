package de.fuberlin.swp2020.mitfahrzentrale.repositories.db1;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;

import java.math.BigDecimal;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AddressRepository extends JpaRepository<Address, UUID> {

    

    @Query("SELECT a FROM Address a where a.latitude = ?1 and a.longitude = ?2")
    Address findByLatitudeAndLongitude(BigDecimal lat, BigDecimal longe);

    @Query("SELECT a FROM Address a where a.postcode = ?1 AND a.street = ?2 AND "
            + "a.streetNumber = ?3 AND a.city = ?4")
    Address findByPostcodeAndStreetAndStreetNumberAndCity(int postcode, String street, String streetnb, String city);



}
