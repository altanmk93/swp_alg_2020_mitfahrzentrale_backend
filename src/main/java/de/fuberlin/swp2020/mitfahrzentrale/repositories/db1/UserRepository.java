package de.fuberlin.swp2020.mitfahrzentrale.repositories.db1;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsUserByUsername(String username);

    User findByUsername(String username);

    @Query("Select u from User u")
    List<User> getAllUser();


    boolean existsById(UUID id);

}
