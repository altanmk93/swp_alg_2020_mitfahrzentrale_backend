package de.fuberlin.swp2020.mitfahrzentrale.repositories.db1;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Commute;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList; 
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository; 
import org.springframework.stereotype.Repository;

 
@Repository
public interface CommuteRepository extends JpaRepository<Commute, UUID> {

    ArrayList<Commute> findByDriver(User driver);

    boolean existsCommuteById(UUID id);

    ArrayList <Commute> getByStartTime(LocalDateTime date);

 


}