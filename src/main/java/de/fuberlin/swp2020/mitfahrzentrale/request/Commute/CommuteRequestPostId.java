package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

import java.util.UUID;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;

public class CommuteRequestPostId {

    private UUID userid;
    private String startTime;
    private Address start;

    public UUID getUserid() {
        return userid;
    }

    public Address getStart() {
        return start;
    }

    public void setStart(Address start) {
        this.start = start;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setUserid(UUID userid) {
        this.userid = userid;
    }
    
}