package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

import java.util.UUID;

public class CommuteRequest {

    private UUID userid;

    public UUID getUserid() {
        return userid;
    }

    public void setUserid(UUID userid) {
        this.userid = userid;
    }
    
}