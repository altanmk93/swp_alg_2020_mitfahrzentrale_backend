package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

import java.util.UUID;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;

public class PossibleCommutes {

    private UUID userid;
    private String date;
    private Address start;
    private Address destination;

    public UUID getUserid() {
        return userid;
    }

    public Address getDestination() {
        return destination;
    }

    public void setDestination(Address destination) {
        this.destination = destination;
    }

    public Address getStart() {
        return start;
    }

    public void setStart(Address start) {
        this.start = start;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUserid(UUID userid) {
        this.userid = userid;
    }
    

    
}