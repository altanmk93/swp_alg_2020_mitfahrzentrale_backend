package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

import java.util.UUID;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;

public class CommuteRequestPost {

    private UUID driverID;
    private int maxPassengers;
    private String startTime; // datetime gives me error. Client is sending strings!
    private Address destination;
    private Address start;

    public Address getDestination() {
        return destination;
    }

    public Address getStart() {
        return start;
    }

    public void setStart(Address start) {
        this.start = start;
    }

    public void setDestination(Address address) {
        this.destination = address;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public UUID getDriverID() {
        return driverID;
    }

    public void setDriverID(UUID driverID) {
        this.driverID = driverID;
    }

 
    
}