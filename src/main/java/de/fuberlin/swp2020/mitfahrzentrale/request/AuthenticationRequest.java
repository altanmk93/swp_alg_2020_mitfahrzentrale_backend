package de.fuberlin.swp2020.mitfahrzentrale.request;

import de.fuberlin.swp2020.mitfahrzentrale.request.Address.AddressRequest;

public class AuthenticationRequest {

    private String email;
    private String password;
    private int carSeats;
    private String firstName;
    private String lastName;
    private String username;
    private final AddressRequest homeAddress;
    private final AddressRequest officeAddress;

    public AuthenticationRequest(String email, String password, int carSeats, String firstName, String lastName,
            AddressRequest homeAddress, AddressRequest officeAddress) {
        this.email = email;
        this.password = password;
        this.carSeats = carSeats;
        this.firstName = firstName;
        this.lastName = lastName;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCarSeats() {
        return carSeats;
    }

    public void setCarSeats(int carSeats) {
        this.carSeats = carSeats;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AddressRequest getHomeAddress() {
        return homeAddress;
    }

    public AddressRequest getOfficeAddress() {
        return officeAddress;
    }
}
