package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

public class CommuteRequestId {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}