package de.fuberlin.swp2020.mitfahrzentrale.request.Commute;

public class CommuteRequestPatch {

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    
}