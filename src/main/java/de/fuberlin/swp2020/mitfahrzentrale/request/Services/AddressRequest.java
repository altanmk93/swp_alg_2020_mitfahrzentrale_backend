package de.fuberlin.swp2020.mitfahrzentrale.request.Services;

import java.math.BigDecimal;

public class AddressRequest {

    private BigDecimal longitutde;
    private BigDecimal latitude;

    public BigDecimal getLatitude() {
        return latitude;
    }

    public BigDecimal getLongitutde() {
        return longitutde;
    }

    public void setLongitutde(BigDecimal longitutde) {
        this.longitutde = longitutde;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }





}