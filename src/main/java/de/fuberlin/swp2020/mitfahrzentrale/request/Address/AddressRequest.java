package de.fuberlin.swp2020.mitfahrzentrale.request.Address;

public class AddressRequest {
    String street;
    String streetNumber;
    String city;
    int postcode;

    public AddressRequest(String street, String streetNumber, String city, int postcode) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getCity() {
        return city;
    }

    public int getPostcode() {
        return postcode;
    }
}
