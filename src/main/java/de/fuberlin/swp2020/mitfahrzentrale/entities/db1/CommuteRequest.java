package de.fuberlin.swp2020.mitfahrzentrale.entities.db1;

 

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "commute_requests")
public class CommuteRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "status")
    private String status;
 
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

 
    @ManyToOne
    @JoinColumn(name = "commute_id", nullable = false)
    private Commute commute;

    public UUID getId(){
        return id;
    }
 
    public String getStatus(){
        return status;
    }

    public User getUser(){
        return user;
    }

    public LocalDateTime getCreatedAt(){
        return createdAt;
    }

    public Address getAddress(){
        return address;
    }

    public Commute getCommute(){
        return commute;
    }

    public void setId(UUID id){
        this.id = id;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setUser(User u){
        this.user = u;
    }

    public void setCreatedAt(LocalDateTime createdAt){
        this.createdAt = createdAt;
    }

    public void setAddress(Address address){
        this.address = address;
    }

    public void setCommute(Commute commute){
        this.commute = commute;
    }


}
