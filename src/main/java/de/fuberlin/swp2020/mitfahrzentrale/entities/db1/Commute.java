package de.fuberlin.swp2020.mitfahrzentrale.entities.db1;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "commutes")
public class Commute {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "creation_date")
    private LocalDateTime date;

    @Column(name = "start_time")
    private LocalDateTime startTime;
 
    @ManyToOne
    @JoinColumn(name = "driver_id", nullable = false)
    private User driver;

    @ManyToMany(mappedBy = "commutes")
    private List<User> passengers;

    @ManyToOne
    @JoinColumn(name = "destination_address_id", referencedColumnName = "id")
    private Address destinationAddress;

    @ManyToOne
    @JoinColumn(name = "start_address_id", referencedColumnName = "id")
    private Address startAddress;

    @Column(name = "max_passengers")
    private int maxPassengers;

    @JsonIgnore
    @OneToMany(mappedBy = "commute")
    private List<CommuteRequest> commuteRequests;

    public Commute() {
    }

    public UUID getId() {
        return id;
    }

    public void setCommuteRequests(List<CommuteRequest> commuteRequests) {
        this.commuteRequests = commuteRequests;
    }

    public List<CommuteRequest> getCommuteRequests() {
        return this.commuteRequests;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public List<User> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<User> passengers) {
        this.passengers = passengers;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Address getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Address startAddress) {
        this.startAddress = startAddress;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }
}
