package de.fuberlin.swp2020.mitfahrzentrale.entities.db1;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.UUID;
import javax.persistence.*;

@Entity
@Table(name = "addresses")
public class Address {

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "postcode")
    private Integer postcode;


    @JsonIgnore
    @OneToMany(mappedBy = "destinationAddress")
    private List<Commute> commuteDestinations;


    @JsonIgnore
    @OneToMany(mappedBy = "startAddress")
    private List<Commute> commuteStarts;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "street_number")
    private String streetNumber;

    @JsonIgnore
    @OneToMany(mappedBy = "homeAddress")
    private List<User> usersHome;

    @JsonIgnore
    @OneToMany(mappedBy = "officeAddress")
    private List<User> usersOffice;

    @JsonIgnore
    @OneToMany(mappedBy = "address")
    private List<CommuteRequest> commuteRequests;

    public Address() {
    }

    public Address(
            double longitude, double latitude, Integer postcode, List<Commute> commuteStarts,
            List<Commute> commuteDestinations, String city, String street, String streetNumber,
            List<CommuteRequest> commuteRequests) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.postcode = postcode;
        this.commuteStarts = commuteStarts;
        this.commuteDestinations = commuteDestinations;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
        this.commuteRequests = commuteRequests;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public List<Commute> getCommuteDestinations() {
        return commuteDestinations;
    }

    public void setCommuteDestinations(List<Commute> commuteDestinations) {
        this.commuteDestinations = commuteDestinations;
    }

    public List<Commute> getCommuteStarts() {
        return commuteStarts;
    }

    public void setCommuteStarts(List<Commute> commuteStarts) {
        this.commuteStarts = commuteStarts;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public List<User> getUsersHome() {
        return usersHome;
    }

    public void setUsersHome(List<User> usersHome) {
        this.usersHome = usersHome;
    }

    public List<User> getUsersOffice() {
        return usersOffice;
    }

    public void setUsersOffice(List<User> usersOffice) {
        this.usersOffice = usersOffice;
    }

    public List<CommuteRequest> getCommuteRequests() {
        return commuteRequests;
    }

    public void setCommuteRequests(List<CommuteRequest> commuteRequests) {
        this.commuteRequests = commuteRequests;
    }
}
