package de.fuberlin.swp2020.mitfahrzentrale.entities.db1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID; 

@Entity
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id; 

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "car_seats")
    private int carSeats;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "commute_passengers", joinColumns = @JoinColumn(name="users_id"), inverseJoinColumns = @JoinColumn(name= "commute_id"))
    private List<Commute> commutes;

    @ManyToOne
    @JoinColumn(name = "home_address_id", nullable = false)
    private Address homeAddress;

    @ManyToOne
    @JoinColumn(name = "office_address_id", nullable = false)
    private Address officeAddress;

    @JsonIgnore
    @OneToMany(mappedBy = "driver")
    private List<Commute> commuteDriver;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<CommuteRequest> commuteRequests;

    public User(
            String email, String username, String firstName, String lastName, String password, String phoneNumber, int carSeats,
            List<Commute> commutes, Address homeAddress, Address officeAddress,  List<Commute> commuteDriver) {
        this.email = email;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.carSeats = carSeats;
        this.commutes = commutes;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
        this.commuteDriver = commuteDriver;
    }

    public User() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCarSeats() {
        return carSeats;
    }

    public void setCarSeats(int carSeats) {
        this.carSeats = carSeats;
    }

    public List<Commute> getCommutes() {
        return commutes;
    }

    public void setCommutes(List<Commute> commutes) {
        this.commutes = commutes;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }

    public List<Commute> getCommuteDriver() {
        return commuteDriver;
    }

    public void setCommuteDriver(List<Commute> commuteDriver) {
        this.commuteDriver = commuteDriver;
    }

    public List<CommuteRequest> getCommuteRequests() {
        return commuteRequests;
    }

    public void setCommuteRequests(List<CommuteRequest> commuteRequests) {
        this.commuteRequests = commuteRequests;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptySet();
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
