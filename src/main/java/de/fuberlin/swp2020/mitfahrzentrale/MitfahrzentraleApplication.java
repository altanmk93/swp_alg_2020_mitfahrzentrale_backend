package de.fuberlin.swp2020.mitfahrzentrale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class MitfahrzentraleApplication {

    public static void main(String[] args) { 
        SpringApplication.run(MitfahrzentraleApplication.class, args);
    }

}
