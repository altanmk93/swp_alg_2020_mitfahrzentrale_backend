package de.fuberlin.swp2020.mitfahrzentrale.services;

import org.springframework.stereotype.Service;
import de.fuberlin.swp2020.mitfahrzentrale.response.routePlannerResponse.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoutePlannerService {
    @PersistenceContext(unitName = "cityRoutingEntityManagerFactory")
    private EntityManager em;

    public Pgr compute_pgr(BigDecimal sourceLat, BigDecimal sourceLon,
                           BigDecimal targetLat, BigDecimal targetLon,
                           double nodeDeltaFilter) {
        Pgr pgr = new Pgr();

        // Variablen vorbereiten
        String queryString = "";

        double routeDistance = 0;
        List<PgrRouteNode> route = new ArrayList<>();

        // Start- und Ziel-Node finden
        PgrNode source = findNodeCloseToCoordinates(sourceLat, sourceLon);
        PgrNode target = findNodeCloseToCoordinates(targetLat, targetLon);

        System.out.println("pgr: Start-Node ermittelt: " + source.getNode().toString());
        System.out.println("pgr: Ziel-Node ermittelt : " + target.getNode().toString());


        // Luftlinie berechnen
        double linearDistance = calculateLinearDistance(sourceLat, sourceLon, targetLat, targetLon);


        // Route finden
        queryString = "SELECT * FROM pgr_dijkstra('SELECT gid as id, source_osm AS source, target_osm AS target, length AS cost FROM ways'," +
                source.getNode().toString() + "," + target.getNode().toString() + ",false)";

        List<Object[]> path = em.createNativeQuery(queryString).getResultList();

        System.out.println("pgr: Query \"" + queryString + "\" liefert " + path.size() + " Ergebnisse.");

        int routeStepCount = path.size();

        // Ausgabe: 0            1           2           3           4           5
        //          Integer      Integer     BigInteger  BigInteger  Double      Double
        //          seq          path_seq    node        edge        cost        agg_cost


        if (nodeDeltaFilter == 0.0) { // Sämtliche Nodes in Route einfügen, ohne ständige IF-Abfrage. Das ist das "Original"
            System.out.println("Kein Filter bzgl. des Abstandes zwischen Wegpunkten");
            for (Object[] node : path) {
                List<Object[]> coords = em.createNativeQuery("select lat, lon from ways_vertices_pgr where osm_id=" +
                        ((BigInteger) node[2]).toString()).getResultList();

                route.add(new PgrRouteNode((int) node[0],             // seq
                        (BigInteger) node[2],             // node
                        (BigDecimal) coords.get(0)[0],    // lat
                        (BigDecimal) coords.get(0)[1],    // lon
                        (Double) node[4],             // cost
                        (Double) node[5]));     // agg_cost

                routeDistance += (double) node[5];
            }
        } else { // Nur Nodes in Route einfügen, wenn sie weit genug voneinander entfernt sind. "Original modifiziert"
            double tempDistance = 0.0;
            int tempStep = 0;

            // Start-Node einfügen weil er in der unteren Iteration übersprungen wird:
            List<Object[]> coords = em.createNativeQuery("select lat, lon from ways_vertices_pgr where osm_id=" + source.getNode().toString()).getResultList();
            PgrRouteNode lastNode = new PgrRouteNode(0,                             // seq
                    source.getNode(),                    // node
                    (BigDecimal) coords.get(0)[0], // lat
                    (BigDecimal) coords.get(0)[1], // lon
                    0.0,                           // cost
                    0.0);                    // agg_cost
            route.add(lastNode);

            // TODO: WIE VERÄNDERN SICH HIERDURCH DIE WERTE FÜR COST UND AGG_COST ? EBENFALLS AUFSUMMIEREN ?
            // Path iterieren und nodes überspringen, wenn der Abstand zum zuletzt eingetragenen Node nicht groß genug ist:
            for (Object[] node : path) {
                tempStep += 1;
                routeDistance += (double) node[5];
                tempDistance += (double) node[5];

                if (tempDistance >= nodeDeltaFilter) {
                    coords = em.createNativeQuery("select lat, lon from ways_vertices_pgr where osm_id=" +
                            ((BigInteger) node[2]).toString()).getResultList();

                    lastNode = new PgrRouteNode(tempStep,                      // Alternative zu seq
                            (BigInteger) node[2],          // node
                            (BigDecimal) coords.get(0)[0], // lat
                            (BigDecimal) coords.get(0)[1], // lon
                            (Double) node[4],          // cost
                            tempDistance);           // agg_cost
                    route.add(lastNode);
                    tempDistance = 0.0;
                } // Node zugefügt


            } // Ende "Original modifiziert" FOR-Schleife

            // Prüfen, ob Zielnode in Route eingefügt wurde und ggf. manuell einfügen (so wie Startpunkt)
            if ((BigInteger) path.get(path.size() - 1)[2] != lastNode.getNode()) {
                coords = em.createNativeQuery("select lat, lon from ways_vertices_pgr where osm_id=" +
                        ((BigInteger) path.get(path.size() - 1)[2]).toString()).getResultList();

                route.add(new PgrRouteNode(tempStep,                                // Alternative zu seq
                        (BigInteger) path.get(path.size() - 1)[2], // node
                        (BigDecimal) coords.get(0)[0],           // lat
                        (BigDecimal) coords.get(0)[1],           // lon
                        (Double) path.get(path.size() - 1)[4], // cost
                        tempDistance));                   // agg_cost
            }


        }


        // Peilung berechnen
        double bearing = calculateBearing(sourceLat, sourceLon, targetLat, targetLon);
        System.out.println("routeDistance: " + routeDistance);
        System.out.println("bearing: " + bearing);


        pgr.setLinearDistance(linearDistance);
        pgr.setBearing(bearing);
        pgr.setRouteDistance(routeDistance);
        pgr.setRouteStepCount(routeStepCount);
        pgr.setRoute(route);

        return pgr;
    }


    public String coordinates2Waypoint(double lat, double lon, String name) {
        String wptString = "  <wpt lat=\"" + lat  + "\" lon=\"" + lon + "\">\n";
        wptString       += "    <name>" + name + "</name>\n";
        return wptString + "  </wpt>\n";
    }


    public String coordinates2Waypoint(Coordinates c, String name) {
        return coordinates2Waypoint(c.getLatitude().doubleValue(), c.getLongitude().doubleValue(), name);
    }


    // Der Wert von einem Radiant, wird benutzt um die Entfernung (Luftlinie) zwischen zwei Punkten zu berechnen.
    private BigDecimal oneRad = new BigDecimal("0.01745329251994329576923690768489");

    // Sucht Nodes in der Nähe der gegebenen Koordinaten
    public PgrNode findNodeCloseToCoordinates(BigDecimal latitude, BigDecimal longitude) {
        String queryString = "SELECT osm_id, lat, lon FROM ways_vertices_pgr ORDER BY the_geom <-> ST_GeometryFromText('POINT(" +
                longitude.toString() + " " + latitude.toString() + ")',4326) LIMIT 1";

        List<Object[]> results = em.createNativeQuery(queryString).getResultList();
        System.out.println("findNodeCloseToCoordinates: query \"" + queryString + "\" liefert " + results.size() + " Ergebnisse.");
        if (results.size() == 0) {
            // error handling here
        }

        return new PgrNode((BigInteger) results.get(0)[0], (BigDecimal) results.get(0)[1], (BigDecimal) results.get(0)[2]);
    }


    // Berechnet die Luftlinie zwischen zwei Punkten
    public double calculateLinearDistance(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2) {  //  lat1, lat2, lon1, lon2: Breite, Länge in Grad
        // Breitengrade ("Querlinien") haben immer einen Abstand von 111,3 km. Der Abstand zweier Längengrade hängt
        // jedoch vom Breitengrad ab: Am Äquator sind sie mit 111,3 km am weitesten voneinander entfernt; in Richtung
        // der Pole geht er gegen Null. Mit dem Kosinus kann man ihren Abstand berechnen: 111.3 * cos(lat)

        double lat = (((lat1.add(lat2)).divide(new BigDecimal("2.00000"))).multiply(oneRad)).doubleValue();
        double dx = 111.3 * Math.cos(lat) * (lon1.subtract(lon2).doubleValue());
        double dy = 111.3 * (lat1.subtract(lat2).doubleValue());
        return Math.sqrt((dx * dx) + (dy * dy)); // in km
    }

    // Gibt die genordete Peilung/Richtung in Grad zurück, wobei 0 = Norden, 90 = Osten,
    // -90 = Westen, 180 = Süden. Kann hier geprüft werden: https://www.igismap.com/map-tool/bearing-angle
    public double calculateBearing(BigDecimal lat1bd, BigDecimal lon1bd, BigDecimal lat2bd, BigDecimal lon2bd) {

        double lat1 = (lat1bd.multiply(oneRad)).doubleValue();
        double lon1 = (lon1bd.multiply(oneRad)).doubleValue();
        double lat2 = (lat2bd.multiply(oneRad)).doubleValue();
        double lon2 = (lon2bd.multiply(oneRad)).doubleValue();

        double coslat2 = Math.cos(lat2); // Weils 2x verwendet wird.

        double deltaLon = lon2 - lon1;
        double X = coslat2 * Math.sin(deltaLon);
        double Y = (Math.cos(lat1) * Math.sin(lat2)) - (Math.sin(lat1) * coslat2 * Math.cos(deltaLon));

        return Math.atan2(X, Y) * 57.295779513;
    }

    // Ermittle den Winkel zwischen den Luftlinien der Startpunkte A,B zum gemeinsamen Ziel
    public double getAngleBetweenSourcesAndTarget(BigDecimal sourceLatA, BigDecimal sourceLonA,
                                                  BigDecimal sourceLatB, BigDecimal sourceLonB,
                                                  BigDecimal targetLat, BigDecimal targetLon) {

        return getAngleBetweenBearings(calculateBearing(sourceLatA, sourceLonA, targetLat, targetLon),
                calculateBearing(sourceLatB, sourceLonB, targetLat, targetLon));
    }

    // Ermittle den Winkel zwischen zwei Peilungen unter Annahme, dass diese aus den Luftlinien zweier Startpunkte mit gemeinsamen Ziel entstammen
    public double getAngleBetweenBearings(double BearingA, double BearingB) {

        if (BearingA < 0) BearingA += 360; // Jetzt entsprechen die Bearing-Werte einem "normalen" Kreis
        if (BearingB < 0) BearingB += 360; // mit 360°, im UZS (mit 0°=Norden, 45° Ost, usw.)

        double dBearing = Math.abs(BearingB - BearingA); // delta Bearing
        if (dBearing > 180)
            return dBearing - 90; // Ist delta über 180, enspricht das dem Aussenwinkel. Wir wollen den Innenwinkel.

        return dBearing;
    }

}
