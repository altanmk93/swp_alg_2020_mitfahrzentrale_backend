package de.fuberlin.swp2020.mitfahrzentrale.services;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;
import de.fuberlin.swp2020.mitfahrzentrale.repositories.db1.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            String msg = String.format("User %s not found", username);
            throw new UsernameNotFoundException(msg);
        }
        return user;
    }


 }
