package de.fuberlin.swp2020.mitfahrzentrale.response;

 
import java.time.LocalDateTime;
import java.util.UUID;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Commute;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;

public class CommuteRequestResponse {

    private UUID id;
    private LocalDateTime createdAt;
    private String state;
    private Commute commute;
    private User user;

    public UUID getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Commute getCommute() {
        return commute;
    }

    public void setCommute(Commute commute) {
        this.commute = commute;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public CommuteRequestResponse() {
    }


    
}