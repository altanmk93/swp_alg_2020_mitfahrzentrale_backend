package de.fuberlin.swp2020.mitfahrzentrale.response.routePlannerResponse;

import java.util.List;

public class Pgr {
    private double linearDistance;         // Distanz Start-Ende Luftlinie
    private double routeDistance = 0;          // Distanz Start-Ende via Route
    private int routeStepCount;         // Anzahl an Nodes in der Route
    private double bearing;                // Peilung
    private List<PgrRouteNode> route;                  // Liste aller Nodes der Route
    private List<Object[]> path;                   // Zwischenspeicher für die Ergebnisse des pgRouting-Query
    private List<Object[]> coords;                 // Helper-List um Koordinaten aus ways_vertices_pgr zu extrahieren. pgrouting gibt diese nicht zurück.


    public double getLinearDistance() {
        return linearDistance;
    }

    public void setLinearDistance(double linearDistance) {
        this.linearDistance = linearDistance;
    }

    public double getRouteDistance() {
        return routeDistance;
    }

    public void setRouteDistance(double routeDistance) {
        this.routeDistance = routeDistance;
    }

    public int getRouteStepCount() {
        return routeStepCount;
    }

    public void setRouteStepCount(int routeStepCount) {
        this.routeStepCount = routeStepCount;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public List<PgrRouteNode> getRoute() {
        return route;
    }

    public void setRoute(List<PgrRouteNode> route) {
        this.route = route;
    }

    public List<Object[]> getPath() {
        return path;
    }

    public void setPath(List<Object[]> path) {
        this.path = path;
    }

    public List<Object[]> getCoords() {
        return coords;
    }

    public void setCoords(List<Object[]> coords) {
        this.coords = coords;
    }


}
