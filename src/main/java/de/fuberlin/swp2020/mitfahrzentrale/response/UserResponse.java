package de.fuberlin.swp2020.mitfahrzentrale.response;

import java.util.UUID;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;

public class UserResponse {

    private UUID id;
    private String firstName;
    private String lastName;
    private int carseats;
    private Address homeAddress;
    private Address officeAddress;
    private String email;
    private String userName;

    public UUID getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public int getCarseats() {
        return carseats;
    }

    public void setCarseats(int carseats) {
        this.carseats = carseats;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UserResponse() {
    }


    
}