package de.fuberlin.swp2020.mitfahrzentrale.response;

import java.util.Date;
import java.util.UUID;
 

public class AuthenticationResponse {

    private final String idToken;
    private final Date expiresIn;
    private UUID userid;

    public AuthenticationResponse(String idToken, Date expiresIn, UUID userid ) {
        this.idToken = idToken;
        this.expiresIn = expiresIn;
        this.userid = userid;
    }

    public UUID getUser() {
        return userid;
    }

    public void setUser(UUID userid) {
        this.userid = userid;
    }

    public String getIdToken() {
        return idToken;
    }

    public Date getExpiresIn() {
        return expiresIn;
    }
}
