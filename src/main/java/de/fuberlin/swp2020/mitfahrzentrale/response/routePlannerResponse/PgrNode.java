package de.fuberlin.swp2020.mitfahrzentrale.response.routePlannerResponse;


import java.math.BigDecimal;
import java.math.BigInteger;

public class PgrNode extends Coordinates {
    private BigInteger node;

    public PgrNode(BigInteger node, BigDecimal latitude, BigDecimal longitude) {
        super(latitude, longitude);
        this.node = node;
    }

    public void setNode(BigInteger node) {
        this.node = node;
    }

    public BigInteger getNode() {
        return node;
    }
}