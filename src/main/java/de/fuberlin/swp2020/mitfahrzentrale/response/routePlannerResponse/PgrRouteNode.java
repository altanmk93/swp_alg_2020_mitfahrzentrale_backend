package de.fuberlin.swp2020.mitfahrzentrale.response.routePlannerResponse;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PgrRouteNode extends PgrNode {
    private int step;
    private double cost;
    private double agg_cost;

    public PgrRouteNode(int step, BigInteger node, BigDecimal latitude, BigDecimal longitude, double cost, double agg_cost) {
        super(node, latitude, longitude);
        this.step = step;
        this.cost = cost;
        this.agg_cost = agg_cost;
    }

    public int getStep() {
        return step;
    }

    public double getCost() {
        return cost;
    }

    public double getAgg_cost() {
        return agg_cost;
    }
}