package de.fuberlin.swp2020.mitfahrzentrale.response;

import java.util.Date;
import java.util.List;

import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.Address;
import de.fuberlin.swp2020.mitfahrzentrale.entities.db1.User;

public class CommutesResponse {

    private long id;
    private Date created_at;
    private User driver;
    private Integer maxPassenger;
    private List<User> passengers;
    private Date startTime;
    private List<Integer> route;
    private Address startAdress;
    private Address destination;

    public long getId() {
        return id;
    }

    public Address getDestination() {
        return destination;
    }

    public void setDestination(Address destination) {
        this.destination = destination;
    }

    public Address getStartAdress() {
        return startAdress;
    }

    public void setStartAdress(Address startAdress) {
        this.startAdress = startAdress;
    }

    public List<Integer> getRoute() {
        return route;
    }

    public void setRoute(List<Integer> route) {
        this.route = route;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<User> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<User> passengers) {
        this.passengers = passengers;
    }

    public Integer getMaxPassenger() {
        return maxPassenger;
    }

    public void setMaxPassenger(Integer maxPassenger) {
        this.maxPassenger = maxPassenger;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CommutesResponse() {
    }


    
}